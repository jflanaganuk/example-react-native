import React from 'react';
import Inputs from '../../src/components/Inputs';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
    const tree = renderer.create(<Inputs />).toJSON();
    expect(tree).toMatchSnapshot();
});