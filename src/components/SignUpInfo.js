import React from 'react';
import { Text, StyleSheet } from 'react-native';

export default class SignUpInfo extends React.Component{
    constructor(props){
        super(props);

        this.handlePress = this.handlePress.bind(this);
    }

    handlePress(){
        alert("You can sign up for an account on your PC at https://uploadr.co.uk");
    }

    render(){
        return(
            <Text style={styles.text} onPress={this.handlePress}>How do I get an account?</Text>
        );
    }
}

const styles = StyleSheet.create({
    text: {
        textDecorationLine: 'underline',
        color: 'white',
        textAlign: 'center'
    }
});