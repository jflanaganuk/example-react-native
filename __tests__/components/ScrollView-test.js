import React from 'react';
import ScrollViewExample from '../../src/components/ScrollView';
import renderer from 'react-test-renderer';

it('renders correctly',() => {
    const tree = renderer.create(<ScrollViewExample/>).toJSON();
    expect(tree).toMatchSnapshot();
});