import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';

export default class List extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            names: [
                {
                    id: 0,
                    name: 'Ben'
                },
                {
                    id: 1,
                    name: 'Susan'
                },
                {
                    id: 2,
                    name: 'Robert'
                },
                {
                    id: 3,
                    name: 'Mary'
                }
            ]
        }

        this.alertItemName = this.alertItemName.bind(this);
    }

    alertItemName(item){
        alert(item.name);
    }

    render(){
        return(
            <View style = {styles.view}>
                {
                    this.state.names.map((item, index) => (
                        <TouchableOpacity
                            key = {item.id}
                            style = {styles.container}
                            onPress = {() => this.alertItemName(item)}>
                            <Text style = {styles.text}>
                                {item.name}
                            </Text>
                        </TouchableOpacity>
                    ))
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        marginTop: 30
    },
    container: {
        padding: 10,
        marginTop: 3,
        backgroundColor: '#d9f9b1',
        alignItems: 'center'
    },
    text: {
        color: '#4f603c'
    }
});