import React from 'react';
import SignUpInfo from '../../src/components/SignUpInfo';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
    const tree = renderer.create(<SignUpInfo />).toJSON();
    expect(tree).toMatchSnapshot();
});