import React from 'react';
import List from '../../src/components/List';
import renderer from 'react-test-renderer';

it('renders correctly',() => {
    const tree = renderer.create(<List />).toJSON();
    expect(tree).toMatchSnapshot();
});