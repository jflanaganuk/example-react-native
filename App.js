import React from 'react';
import { View } from 'react-native';
import List from './src/components/List';
import Inputs from './src/components/Inputs';
import ScrollViewExample from './src/components/ScrollView';
import ImageExample from './src/components/image_example';
import SignUpInfo from './src/components/SignUpInfo';

export default class App extends React.Component {
	render() {
		return (
			<View style = {{backgroundColor: '#4977c1', height: '100%'}}>
				<ImageExample/>
				<Inputs />
				<SignUpInfo />
			</View>
		);
	}
}
