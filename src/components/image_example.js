import React from 'react';
import { Image } from 'react-native';

export default class ImagesExample extends React.Component{
    render(){
        return <Image 
                    source = {require('../../assets/uploadr.png')}
                    style = {{width: '100%', marginTop: 30}}
                    resizeMode="contain"
                    />
    }
}