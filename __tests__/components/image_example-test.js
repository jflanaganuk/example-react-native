import React from 'react';
import ImagesExample from '../../src/components/image_example';
import renderer from 'react-test-renderer';

it('renders correctly',() => {
    const tree = renderer.create(<ImagesExample />).toJSON();
    expect(tree).toMatchSnapshot();
});